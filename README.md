# EasyHTTP
A small library to make HTTP requests. I implemented several versions:

* **Version 1** uses the [XMLHttpRequest](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest) API.
* **Version 2** uses the  [fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)

It uses no JS frameworks, just vanilla JavaScript.

Click [here](https://dummylanguages.gitlab.io/easyhttp) to see it **live**!

## Dependencies:

* [JSONPlaceholder fake API](https://jsonplaceholder.typicode.com/)
