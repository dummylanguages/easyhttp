class EasyHTTP {
  // Make a GET request
  async get(url) {
    const response  = await fetch(url);
    const data      = await response.json();
    return data;
  }
  // Make a POST request
  async post(url, data) {
    const req = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    const reqData = await req.json();
    return reqData;
  }

  // Make a PUT request
  /*
  ** Make a PUT request (Modify a post)
  ** Some REST apis return the data back to the
  ** client as part of the http.responseText property.
  ** (We're not actually modifying anything in jsonplaceholder)
  */
  async put(url, data) {
    const req = await fetch(url, {
        method: 'PUT',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    const reqData = await req.json();
    return reqData;
  }


  /*
  ** Make a DELETE request
  ** Some REST apis return the data back to the
  ** client as part of the http.responseText property.
  ** In this case, the response should be an empty object.
  ** (We're not actually DELETING anything in jsonplaceholder)
  */
  async delete(url) {
    const req = await fetch(url, {
        method: 'DELETE',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    const reqData = await 'Resource Deleted!';
    return reqData;
  }
}


