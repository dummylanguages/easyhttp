const http = new EasyHTTP;
// const users = http.get('https://jsonplaceholder.typicode.com/users')
//   .then(data => console.log(data))
//   .catch(error => console.log(error));

// GET a single post (for example with id = 3)


/*
** POST a post
** Some REST apis return the data back to the
** client as part of the http.responseText property.
** (We're not actually POSTING anything in jsonplaceholder)
*/
// Create data for a post
const data = {
  name: 'John Doe',
  username: 'nasty-dog-1980',
  email: 'nastydog1980@gmail.com'
};
// http.post('https://jsonplaceholder.typicode.com/users', data)
//   .then(data => console.log(data))
//   .catch(error => console.log(error));

/*
** PUT: Modify a post
** Some REST apis return the data back to the
** client as part of the http.responseText property.
** (We're not actually modifying anything in jsonplaceholder)
*/
http.put('https://jsonplaceholder.typicode.com/users/1', data)
  .then(data => console.log(data))
  .catch(error => console.log(error));

/*
** DELETE a post
** Some REST apis return the data back to the
** client as part of the http.responseText property.
** (We're not actually DELETING anything in jsonplaceholder)
*/
http.delete('https://jsonplaceholder.typicode.com/users/1')
  .then(data => console.log(data))
  .catch(error => console.log(error));
