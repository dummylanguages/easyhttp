function EasyHTTP() {
  this.http = new XMLHttpRequest();
}

// Make a GET request
EasyHTTP.prototype.get = function (url, callback) {
  this.http.open('GET', url, true);
  let self = this;
  this.http.onload = function () {
    if (self.http.status === 200) {
      callback(null, self.http.responseText);
    } else {
      callback(`Error: ${self.http.status}`);

    }
  }
  this.http.send();
}

// Make a POST request
EasyHTTP.prototype.post = function (url, data, callback) {
  this.http.open('POST', url, true);
  this.http.setRequestHeader('Content-type', 'application/json');
  let self = this;
  this.http.onload = function () {
    callback(null, self.http.responseText);
  }
  this.http.send(JSON.stringify(data));
}

/*
** Make a PUT request (Modify a post)
** Some REST apis return the data back to the
** client as part of the http.responseText property.
** (We're not actually modifying anything in jsonplaceholder)
*/
EasyHTTP.prototype.put = function (url, data, callback) {
  this.http.open('PUT', url, true);
  this.http.setRequestHeader('Content-type', 'application/json');
  let self = this;
  this.http.onload = function () {
    callback(null, self.http.responseText);
  }
  this.http.send(JSON.stringify(data));
}

/*
** Make a DELETE request
** Some REST apis return the data back to the
** client as part of the http.responseText property.
** In this case, the response should be an empty object.
** (We're not actually DELETING anything in jsonplaceholder)
*/
EasyHTTP.prototype.delete = function (url, callback) {
  this.http.open('DELETE', url, true);
  let self = this;
  this.http.onload = function () {
    if (self.http.status === 200) {
      // callback(null, 'Post Successfully Deleted!');
      callback(null, self.http.responseText);
    } else {
      callback(`Error: ${self.http.status}`);
    }
  }
  this.http.send();
}
