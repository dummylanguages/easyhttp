class EasyHTTP {
  // Make a GET request
  get(url) {
    return new Promise((resolve, reject) => {
      fetch(url)
        .then(resp => resp.json())
        .then(data => resolve(data))
        .catch(error => reject(error));
    });
  }
  // Make a POST request
  post(url, data) {
    return new Promise((resolve, reject) => {
      fetch(url, {
        method: 'POST',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(data)
      })
        .then(resp => resp.json())
        .then(data => resolve(data))
        .catch(error => reject(error));
    });
  }

  // Make a PUT request
  /*
  ** Make a PUT request (Modify a post)
  ** Some REST apis return the data back to the
  ** client as part of the http.responseText property.
  ** (We're not actually modifying anything in jsonplaceholder)
  */
  put(url, data) {
    return new Promise((resolve, reject) => {
      fetch(url, {
        method: 'PUT',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(data)
      })
        .then(resp => resp.json())
        .then(data => resolve(data))
        .catch(error => reject(error));
    });
  }


  /*
  ** Make a DELETE request
  ** Some REST apis return the data back to the
  ** client as part of the http.responseText property.
  ** In this case, the response should be an empty object.
  ** (We're not actually DELETING anything in jsonplaceholder)
  */
  delete(url) {
    return new Promise((resolve, reject) => {
      fetch(url, {
        method: 'DELETE',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(data)
      })
        .then(resp => resp.json())
        .then(() => resolve('Resource Successfully Deleted!'))
        .catch(error => reject(error));
    });
  }
}


