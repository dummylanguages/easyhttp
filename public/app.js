const http = new EasyHTTP();

// GET posts
// http.get('https://jsonplaceholder.typicode.com/posts',
// function (err, posts) {
//   if (err)
//     console.log(err);
//   else
//     console.log(posts);
// });

// GET a single post (for example with id = 3)
// http.get('https://jsonplaceholder.typicode.com/posts/3',
// function (err, post) {
//   if (err)
//     console.log(err);
//   else
//     console.log(post);
// });

// Create data for a post
const data = {
  title: 'New post',
  body: 'Some amazing content goes here!'
};

/*
** POST a post
** Some REST apis return the data back to the
** client as part of the http.responseText property.
** (We're not actually POSTING anything in jsonplaceholder)
*/
// http.post('https://jsonplaceholder.typicode.com/posts',
//   data,
//   function (error, responsePost) {
//     if (error)
//       console.log(error);
//     else
//       console.log(responsePost);
// });

/*
** Modify a post
** Some REST apis return the data back to the
** client as part of the http.responseText property.
** (We're not actually modifying anything in jsonplaceholder)
*/
// http.put('https://jsonplaceholder.typicode.com/posts/1',
//   data,
//   function (error, responsePost) {
//     if (error)
//       console.log(error);
//     else
//       console.log(responsePost);
// });

/*
** DELETE a post
** Some REST apis return the data back to the
** client as part of the http.responseText property.
** (We're not actually DELETING anything in jsonplaceholder)
*/
http.delete('https://jsonplaceholder.typicode.com/posts/1',
  function (error, response) {
    if (error) {
      console.log(error);
    } else if (response === '{}') {
      console.log('Post deleted!');
    }
});
